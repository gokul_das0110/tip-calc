package com.example.tiptime

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.tiptime.databinding.ActivityMainBinding
import java.text.NumberFormat

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding=ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.calcButton.setOnClickListener { calculateTip() }


    }
    private fun calculateTip(){
        val stringInTextField=binding.costText.text.toString()
        val cost=stringInTextField.toDouble()
        val selectedId=binding.radioGroup.checkedRadioButtonId
        val tipPercentage= when(selectedId){
            R.id.twenty_per -> 0.20
            R.id.ten_per ->0.15
            else -> 0.10
        }
        var tip=tipPercentage*cost
        if (binding.ischecked.isChecked) {
            tip = kotlin.math.ceil(tip)
        }
        val formattedTip = NumberFormat.getCurrencyInstance().format(tip)
        binding.result.text = getString(R.string.tip_amount, formattedTip)
    }
}